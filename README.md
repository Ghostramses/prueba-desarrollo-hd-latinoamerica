# Administrador de licencias

El administrador te permite crear, activar, desactivar y renovar licencias

## Instrucciones

### Environment

Agregar los campos:

-   API_TOKEN (Token que se obtiene en el registro de la API de HD Latinoamerica)
-   API_USER (Usuario que se obtiene en el registro de la API de HD Latinoamerica)
-   API_PASS (Contraseña que se obtiene en el registro de la API de HD Latinoamerica)

A el archivo .env que genera laravel, no olvidar los campos de usuario de la base de datos, contraseña de la base de datos y nombre de la base de datos a la que se conectará el framework.

### Iniciar el back end

Para iniciar el back end ir a la carpeta api/ y ejecutar el comando `php artisan serve`, si se cambiará el puerto por defecto lea la seccion [configuración de proxy del front end.](#proxy-front-end)

### Iniciar el front end

Para iniciar el front end ir a la carpeta licencias/ y ejecutar el comando `npm start` o `npm run start`.

### Proxy Front end

En la carpeta licencias/ se encuentra un archivo con el nombre proxy.conf.json debe alterar el archivo en la llave target cambiando el puerto de la url por el puerto en el que se encuentra ejecutando el back end.

Si realizó este cambio mientras ejecutaba el front end, debe reiniciar el servicio de front end para que se vean aplicados los cambios.

### Vista en raíz

Se agregó la vista de angular en la raíz de laravel, sólo ejecuta el comando `php artisan serve` y accede normalmente desde el navegador.
