<?php

namespace App\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Container\ContainerInterface;

class LicenseController
{
    private ContainerInterface $container;

    public function __construct(ContainerInterface $c)
    {
        $this->container = $c;
    }

    public function getAll(Request $request, Response $response, $args)
    {
        $db = $this->container->get("db");
        $query = $db->query("SELECT id_lic FROM vta_licencia");
        $licensesID = ($query->fetch_all());
        $out = array();
        foreach ($licensesID as $licenseID) {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://practica.hdlatinoamerica.com/api/get',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => array('id_lic' => $licenseID[0]),
                CURLOPT_HTTPHEADER => array(
                    'token: ' . $_ENV["API_TOKEN"],
                    'Authorization: Basic ' . base64_encode($_ENV["API_USER"] . ":" . $_ENV["API_PASS"])
                ),
            ));

            $APIResponse = curl_exec($curl);
            $out[] = json_decode($APIResponse, true)["response"];
            curl_close($curl);
        }

        $response->getBody()->write(json_encode($out));
        $query->free();
        $db->close();
        return $response->withHeader("Content-type", "application/json")->withStatus(200);
    }

    public function newLicense(Request $request, Response $response, $args)
    {
        $parsedBody = $request->getParsedBody();
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://practica.hdlatinoamerica.com/api/create',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('name' => $parsedBody["name"], "vig" => $parsedBody["vig"], "prod" => $parsedBody["prod"]),
            CURLOPT_HTTPHEADER => array(
                'token: ' . $_ENV["API_TOKEN"],
                'Authorization: Basic ' . base64_encode($_ENV["API_USER"] . ":" . $_ENV["API_PASS"])
            ),
        ));

        $APIResponse = curl_exec($curl);
        curl_close($curl);

        $db = $this->container->get("db");
        $stmt = $db->prepare("CALL agregarLicencia(?,@id)");
        $stmt->bind_param("s", json_decode($APIResponse, true)["response"]["id"]);
        $stmt->execute();
        $result = $db->query("SELECT @id as idout");
        $stmt->close();
        $result->free();
        $db->close();

        $response->getBody()->write($APIResponse);
        return $response->withHeader("Content-type", "application/json")->withStatus(200);
    }

    public function activateLicense(Request $request, Response $response, $args)
    {
        $parsedBody = $request->getParsedBody();

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://practica.hdlatinoamerica.com/api/activate',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('id_lic' => $parsedBody["id_lic"]),
            CURLOPT_HTTPHEADER => array(
                'token: ' . $_ENV["API_TOKEN"],
                'Authorization: Basic ' . base64_encode($_ENV["API_USER"] . ":" . $_ENV["API_PASS"])
            ),
        ));

        $APIResponse = curl_exec($curl);
        curl_close($curl);

        $response->getBody()->write($APIResponse);
        return $response->withHeader("Content-type", "application/json")->withStatus(200);
    }

    public function renewLicense(Request $request, Response $response, $args)
    {
        $parsedBody = $request->getParsedBody();

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://practica.hdlatinoamerica.com/api/renew',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('id_lic' => $parsedBody["id_lic"], "vig" => $parsedBody["vig"]),
            CURLOPT_HTTPHEADER => array(
                'token: ' . $_ENV["API_TOKEN"],
                'Authorization: Basic ' . base64_encode($_ENV["API_USER"] . ":" . $_ENV["API_PASS"])
            ),
        ));

        curl_exec($curl);
        curl_close($curl);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://practica.hdlatinoamerica.com/api/get',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('id_lic' => $parsedBody["id_lic"]),
            CURLOPT_HTTPHEADER => array(
                'token: ' . $_ENV["API_TOKEN"],
                'Authorization: Basic ' . base64_encode($_ENV["API_USER"] . ":" . $_ENV["API_PASS"])
            ),
        ));

        $APIResponse = curl_exec($curl);

        $response->getBody()->write($APIResponse);
        return $response->withHeader("Content-type", "application/json")->withStatus(200);
    }

    public function deactivateLicense(Request $request, Response $response, $args)
    {
        $parsedBody = $request->getParsedBody();

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://practica.hdlatinoamerica.com/api/disabled',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('id_lic' => $parsedBody["id_lic"]),
            CURLOPT_HTTPHEADER => array(
                'token: ' . $_ENV["API_TOKEN"],
                'Authorization: Basic ' . base64_encode($_ENV["API_USER"] . ":" . $_ENV["API_PASS"])
            ),
        ));
        $APIResponse = curl_exec($curl);
        curl_close($curl);

        $response->getBody()->write($APIResponse);
        return $response->withHeader("Content-type", "application/json")->withStatus(200);
    }
}
