<?php

use Psr\Container\ContainerInterface;

$container->set("db", function (ContainerInterface $c) {
    $config = $c->get("db_settings");
    return new mysqli($config->DB_HOST, $config->DB_USER, $config->DB_PASS, $config->DB_NAME, $config->DB_PORT);
});
