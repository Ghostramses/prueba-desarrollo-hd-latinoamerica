<?php

use Slim\Routing\RouteCollectorProxy;

$app->group("/v1", function (RouteCollectorProxy $group) {
    $group->get("/licencias", "App\Controllers\LicenseController:getAll");
    $group->post("/licencias", "App\Controllers\LicenseController:newLicense");
    $group->post("/licencias/activate", "App\Controllers\LicenseController:activateLicense");
    $group->post("/licencias/renew", "App\Controllers\LicenseController:renewLicense");
    $group->post("/licencias/deactivate", "App\Controllers\LicenseController:deactivateLicense");
});
