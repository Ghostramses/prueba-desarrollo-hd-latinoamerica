<?php

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . "/../../");
$dotenv->load();

$container->set("db_settings", function () {
    return (object)[
        "DB_NAME" => $_ENV["DB_NAME"],
        "DB_USER" => $_ENV["DB_USER"],
        "DB_PASS" => $_ENV["DB_PASS"],
        "DB_HOST" => $_ENV["DB_HOST"],
        "DB_PORT" => $_ENV["DB_PORT"]
    ];
});
