<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LicenciasController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get("licencias","App\Http\Controllers\LicenciasController@getAll");
Route::post("licencias","App\Http\Controllers\LicenciasController@newLicense");
Route::post("licencias/activate","App\Http\Controllers\LicenciasController@activateLicense");
Route::post("licencias/renew","App\Http\Controllers\LicenciasController@renewLicense");
Route::post("licencias/deactivate","App\Http\Controllers\LicenciasController@deactivateLicense");