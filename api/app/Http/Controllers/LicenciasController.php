<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Licencia;
use DB;

class LicenciasController extends Controller
{
    public function getAll(){
        $licensesID = DB::table("licencias")->select("id_lic")->get();
        $out = array();
        foreach($licensesID as $licenseID){
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://practica.hdlatinoamerica.com/api/get',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => array('id_lic' => $licenseID->id_lic),
                CURLOPT_HTTPHEADER => array(
                    'token: ' . $_ENV["API_TOKEN"],
                    'Authorization: Basic ' . base64_encode($_ENV["API_USER"] . ":" . $_ENV["API_PASS"])
                ),
            ));

            $APIResponse = json_decode(curl_exec($curl),true);
            if(isset($APIResponse["response"])){
                $out[] = $APIResponse["response"];
            }
            curl_close($curl);
        }
        return $out;
    }

    public function newLicense(Request $request){
        $parsedBody = $request->all();
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://practica.hdlatinoamerica.com/api/create',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('name' => $parsedBody["name"], "vig" => $parsedBody["vig"], "prod" => $parsedBody["prod"]),
            CURLOPT_HTTPHEADER => array(
                'token: ' . $_ENV["API_TOKEN"],
                'Authorization: Basic ' . base64_encode($_ENV["API_USER"] . ":" . $_ENV["API_PASS"])
            ),
        ));

        $APIResponse = json_decode(curl_exec($curl),true);
        curl_close($curl);
        if($APIResponse["code"]==201){
            DB::table("licencias")->insert(["id_lic"=>$APIResponse["response"]["id"]]);
        }
        return $APIResponse;
    }

    public function activateLicense(Request $request){
        $parsedBody = $request->all();
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://practica.hdlatinoamerica.com/api/activate',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('id_lic' => $parsedBody["id_lic"]),
            CURLOPT_HTTPHEADER => array(
                'token: ' . $_ENV["API_TOKEN"],
                'Authorization: Basic ' . base64_encode($_ENV["API_USER"] . ":" . $_ENV["API_PASS"])
            ),
        ));

        $APIResponse = json_decode(curl_exec($curl),true);
        curl_close($curl);

        
        return $APIResponse;
    }

    public function renewLicense(Request $request){
        $parsedBody = $request->all();

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://practica.hdlatinoamerica.com/api/renew',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('id_lic' => $parsedBody["id_lic"], "vig" => $parsedBody["vig"]),
            CURLOPT_HTTPHEADER => array(
                'token: ' . $_ENV["API_TOKEN"],
                'Authorization: Basic ' . base64_encode($_ENV["API_USER"] . ":" . $_ENV["API_PASS"])
            ),
        ));

        curl_exec($curl);
        curl_close($curl);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://practica.hdlatinoamerica.com/api/get',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('id_lic' => $parsedBody["id_lic"]),
            CURLOPT_HTTPHEADER => array(
                'token: ' . $_ENV["API_TOKEN"],
                'Authorization: Basic ' . base64_encode($_ENV["API_USER"] . ":" . $_ENV["API_PASS"])
            ),
        ));

        $APIResponse = json_decode(curl_exec($curl),true);
        return $APIResponse;
    }

    public function deactivateLicense(Request $request){
        $parsedBody = $request->all();
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://practica.hdlatinoamerica.com/api/disabled',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('id_lic' => $parsedBody["id_lic"]),
            CURLOPT_HTTPHEADER => array(
                'token: ' . $_ENV["API_TOKEN"],
                'Authorization: Basic ' . base64_encode($_ENV["API_USER"] . ":" . $_ENV["API_PASS"])
            ),
        ));
        $APIResponse = json_decode(curl_exec($curl),true);
        curl_close($curl);

        return $APIResponse;
    }
}
