import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Licencia } from './licencia/licencia.model';

@Injectable()
export class LicenciasService {
	public licencias: Licencia[] = [];

	constructor(private httpClient: HttpClient) {}

	public setLicencias(licencias: Licencia[]) {
		this.licencias = licencias;
		this.licencias.forEach((_, index) => {
			this.setExpiration(index);
		});
	}

	public getLicencias(): Observable<Object> {
		return this.httpClient.get('/api/licencias');
	}

	public crearLicencia(name: string, vig: string, prod: string): void {
		let body = new URLSearchParams();
		body.set('name', name);
		body.set('vig', vig);
		body.set('prod', prod);
		let options = {
			headers: new HttpHeaders().set(
				'Content-type',
				'application/x-www-form-urlencoded'
			)
		};
		this.httpClient
			.post('/api/licencias', body.toString(), options)
			.subscribe(res => {
				const data: any = <any>res;
				if (this.licencias == null) {
					this.licencias = [];
				}
				this.licencias.push(<Licencia>data.response);
			});
	}

	public activarLicencia(index: number): void {
		let body = new URLSearchParams();
		body.set('id_lic', this.licencias[index].id);
		let options = {
			headers: new HttpHeaders().set(
				'Content-type',
				'application/x-www-form-urlencoded'
			)
		};
		this.httpClient
			.post('/api/licencias/activate', body.toString(), options)
			.subscribe(res => {
				const data: any = <any>res;
				this.licencias[index].status = data.response.status;
				this.licencias[
					index
				].time_active = data.response.time_active.toString();
				this.licencias[
					index
				].time_vig = data.response.time_vig.toString();
				this.setExpiration(index);
			});
	}

	public renovarLicencia(index: number): void {
		let body = new URLSearchParams();
		body.set('id_lic', this.licencias[index].id);
		body.set('vig', this.licencias[index].vig);
		let options = {
			headers: new HttpHeaders().set(
				'Content-type',
				'application/x-www-form-urlencoded'
			)
		};
		this.httpClient
			.post('/api/licencias/renew', body.toString(), options)
			.subscribe(res => {
				const data: any = <any>res;
				this.licencias[index].status = data.response.status;
				this.licencias[
					index
				].time_active = data.response.time_active.toString();
				this.licencias[
					index
				].time_vig = data.response.time_vig.toString();
				this.setExpiration(index);
			});
	}

	public desactivarLicencia(index: number): void {
		let body = new URLSearchParams();
		body.set('id_lic', this.licencias[index].id);
		let options = {
			headers: new HttpHeaders().set(
				'Content-type',
				'application/x-www-form-urlencoded'
			)
		};

		this.httpClient
			.post('/api/licencias/deactivate', body.toString(), options)
			.subscribe(res => {
				const data: any = <any>res;
				this.licencias[index].status = data.response.status.toString();
			});
	}

	private setExpiration(index: number): void {
		const unixTime = Math.floor(Date.now() / 1000);
		if (parseInt(this.licencias[index].time_vig) > unixTime)
			setTimeout(() => {
				if (this.licencias[index].status == '2')
					this.licencias[index].status = '4';
			}, (parseInt(this.licencias[index].time_vig) - unixTime) * 1000);
	}
}
