import { Component, OnInit } from '@angular/core';
import { LicenciasService } from '../licencias.service';

@Component({
	selector: 'app-formulario',
	templateUrl: './formulario.component.html'
})
export class FormularioComponent implements OnInit {
	public name: string = '';
	public vig: string = '';
	public prod: string = '';

	constructor(private licenciasService: LicenciasService) {}

	ngOnInit(): void {}

	public crearLicencia(): void {
		if (this.name && this.vig && this.prod) {
			this.licenciasService.crearLicencia(this.name, this.vig, this.prod);
			this.name = this.vig = this.prod = '';
		}
	}
}
