import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { FormularioComponent } from './formulario/formulario.component';
import { LicenciaComponent } from './licencia/licencia.component';
import { LicenciasService } from './licencias.service';

@NgModule({
  declarations: [AppComponent, FormularioComponent, LicenciaComponent],
  imports: [BrowserModule, FormsModule, HttpClientModule],
  providers: [LicenciasService],
  bootstrap: [AppComponent],
})
export class AppModule {}
