import { Component, OnInit } from '@angular/core';
import { LicenciasService } from '../licencias.service';
import { Licencia } from './licencia.model';

@Component({
	selector: 'app-licencia',
	templateUrl: './licencia.component.html'
})
export class LicenciaComponent implements OnInit {
	public licencias: Licencia[] = [];
	public index: number = -1;

	private vigDict: Map<string, string> = new Map<string, string>([
		['1', '5 minutos'],
		['2', '10 minutos'],
		['3', '15 minutos'],
		['4', '20 minutos']
	]);
	private prodDict: Map<string, string> = new Map<string, string>([
		['1', '5 equipos'],
		['2', '10 equipos'],
		['3', '15 equipos'],
		['4', '20 equipos']
	]);
	private statusDict: Map<string, string> = new Map<string, string>([
		['1', 'Inactiva'],
		['2', 'Activa'],
		['3', 'Desactivada'],
		['4', 'Expirada']
	]);

	constructor(private licenciasService: LicenciasService) {}

	ngOnInit(): void {
		this.licenciasService.getLicencias().subscribe(licencias => {
			this.licencias = <Licencia[]>licencias;
			this.licenciasService.setLicencias(<Licencia[]>licencias);
		});
	}

	public getVig(licencia: Licencia): string {
		return this.vigDict.get(licencia.vig.toString())!;
	}

	public getProd(licencia: Licencia): string {
		return this.prodDict.get(licencia.prod.toString())!;
	}

	public getStatus(licencia: Licencia): string {
		return this.statusDict.get(licencia.status.toString())!;
	}

	public activarLicencia(index: number): void {
		this.licenciasService.activarLicencia(index);
	}
	public desactivarLicencia(index: number): void {
		this.licenciasService.desactivarLicencia(index);
	}
	public renovarLicencia(index: number): void {
		this.licenciasService.renovarLicencia(index);
	}
}
