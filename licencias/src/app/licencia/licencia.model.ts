export class Licencia {
	constructor(
		public id: string,
		public code: string,
		public name: string,
		public vig: string,
		public prod: string,
		public status: string,
		public time_active: string,
		public time_vig: string
	) {}
}
